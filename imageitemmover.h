#ifndef IMAGEITEMMOVER_H
#define IMAGEITEMMOVER_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

class ImageItemMover : public QWidget
{
    Q_OBJECT
public:
    explicit ImageItemMover(QWidget *parent = nullptr, int id = 0);
    void setId(int id);
    void setDisabledUp(bool disabled);
    void setDisabledDown(bool disabled);

private:
    QPushButton* upArrow;
    QLineEdit* idEdit;
    int id;
    QPushButton* downArrow;

signals:

};

#endif // IMAGEITEMMOVER_H
